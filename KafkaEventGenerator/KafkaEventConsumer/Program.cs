﻿using System;
using Confluent.Kafka;

namespace KafkaEventConsumer
{
    class Program
    {
        static ConsumerConfig config = new ConsumerConfig
        {
            BootstrapServers = "192.168.1.37:9092, 192.168.1.37:9093, 192.168.1.37:9094",
            GroupId = "webEvent",
            AutoOffsetReset = AutoOffsetReset.Earliest,
        };
        static string topic = "webEvent";

        private static void Listenner()
        {
            using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
            {
                consumer.Subscribe(topic);
                bool cancel = false;

                while (!cancel)
                {
                    var consumeResult = consumer.Consume();
                    Console.WriteLine(consumeResult.Message.Value + " - " + consumeResult.Partition);
                }

                consumer.Close();
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Listen Kafka Topic : " + topic);
            Listenner();
        }
    }
}
