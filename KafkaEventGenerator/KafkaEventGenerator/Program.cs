﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace KafkaEventGenerator
{
    class Program
    {
        static ProducerConfig config = new ProducerConfig
        {
            BootstrapServers = "192.168.1.37:9092, 192.168.1.37:9093, 192.168.1.37:9094",
        };
        static string topic = "webEvent";

        private async static Task GenerateEvents(int num)
        {
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                for (int i = 0; i < num; i++)
                {
                    Console.WriteLine("Event : " + i);
                    await producer.ProduceAsync(topic, new Message<Null, string> { Value = "Event : " + i });
                    Thread.Sleep(1000);
                }
            }
        }
        
        static async Task Main(string[] args)
        {
            Console.WriteLine("Events Generation ... . -1 to Exit");
            int numberMessage = 10;
            while (numberMessage != -1)
            {
                numberMessage = int.Parse(Console.ReadLine());
                await GenerateEvents(numberMessage);
            }
        }
    }
}
 