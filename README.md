# KafkaEvents

## Participants

* MOLINA Thibaut
* SAUREL Marius
* MARTYNOVA Natalia
* DEROUICH Bilâl
* LIENARD Antoine

## Limitation

- Nous avons une grande perte de performance dans si Kafka doit modifier le message.
- Nous avons aussi une perte de performance avec l'augmentation du nombre de noeuds et la quantité de données.


## Run

Find console .exe in : KafkaEvent[X]\bin\Debug\netcoreapp3.1\KafkaEvent[X].exe
